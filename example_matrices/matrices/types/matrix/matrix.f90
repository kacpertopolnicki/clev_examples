program matrix
	implicit none
	character * 2000 :: arg
	character * 2000 :: envvalue

	integer :: msize
	double precision , allocatable :: mreal(: , :)
	double precision , allocatable :: mimag(: , :)

	integer :: numarg

	numarg = iargc()

	if(numarg .ne. 1) then
		call exit(1)
	end if
	call getarg(1 , arg)

	open(11 , file = trim(arg) , status = 'old')
		read(11 , *) msize
		allocate(mreal(msize , msize))
		allocate(mimag(msize , msize))
		read(11 , *) mreal
		read(11 , *) mimag
	close(11)

	deallocate(mreal)
	deallocate(mimag)
	call getenv("CLUSTER_OK" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)
end program matrix
